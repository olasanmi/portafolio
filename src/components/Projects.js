import React from 'react';
import {
    Box,
    chakra,
    Container,
    Image,
    Text,
    IconButton,
    HStack,
    VStack,
    Flex,
    useColorModeValue,
    useBreakpointValue,
    Tag
} from '@chakra-ui/react';
import { FaLink   } from 'react-icons/fa'
import fzchat from '../images/fzchat.png'
import onexfy from '../images/onexfy.png'
import dra from '../images/dra.png'

const milestones = [
    {
        id: 1,
        date: '2023',
        title: 'Fz Chat Center App',
        description: `This application is an omnidirectional message center which allows this client to have in a single channel all the means of communication that they use to give sales advisors easier access to the reads obtained from the different digital guidelines.`,
        link: 'https://app.fzautos.co/',
        img: fzchat
    },
    {
        id: 2,
        date: '2023',
        title: 'Onexfy',
        description: `Onexfy is an ERP software for ecommerce which is responsible for keeping the inventory of web pages updated in real time, in addition to having accounting functionalities, pos system, dropshipper system.`,
        link: 'https://app.onexfy.com/',
        img: onexfy
    },
    {
        id: 3,
        date: '2022',
        title: 'Diana pulgarin Web App',
        description: `App and website for Dr. Diana Thumbin, who is one of the most recognized dentists in all of Colombia, appointment scheduling, online payments, real cases and real results..`,
        link: 'https://www.dradianapulgarin.com/',
        img: dra
    },
];

const Projects = () => {
    const isMobile = useBreakpointValue({ base: true, md: false });
    const isDesktop = useBreakpointValue({ base: false, md: true });

    return (
        <Container maxWidth="7xl" maxHeight={'3xl'} overflowX={'auto'} p={{ base: 2, sm: 10 }}
            my={2}
            css={{
                '&::-webkit-scrollbar': {
                    marginTop: '8px',
                    width: '8px',
                    height: '8px',
                },
                '&::-webkit-scrollbar-track': {
                    marginTop: '8px',
                    width: '2px',
                    height: '4px',
                },
                '&::-webkit-scrollbar-thumb': {
                    marginTop: '8px',
                    background: '#121212',
                    borderRadius: '12px',
                },
            }}
        >

            {milestones.map((milestone) => (
                <Flex key={milestone.id} mb="10px">
                    {/* Desktop view(left card) */}
                    {isDesktop && milestone.id % 2 === 0 && (
                        <>
                            <EmptyCard />
                            <LineWithDot />
                            <Card {...milestone} />
                        </>
                    )}

                    {/* Mobile view */}
                    {isMobile && (
                        <>
                            <LineWithDot />
                            <Card {...milestone} />
                        </>
                    )}

                    {/* Desktop view(right card) */}
                    {isDesktop && milestone.id % 2 !== 0 && (
                        <>
                            <Card {...milestone} />

                            <LineWithDot />
                            <EmptyCard />
                        </>
                    )}
                </Flex>
            ))}
        </Container>
    );
};



const Card = ({ id, title, description, date, link, img }) => {
    // For even id show card on left side
    // For odd id show card on right side
    const isEvenId = id % 2 === 0;
    let borderWidthValue = isEvenId ? '15px 15px 15px 0' : '15px 0 15px 15px';
    let leftValue = isEvenId ? '-15px' : 'unset';
    let rightValue = isEvenId ? 'unset' : '-15px';

    const isMobile = useBreakpointValue({ base: true, md: false });
    if (isMobile) {
        leftValue = '-15px';
        rightValue = 'unset';
        borderWidthValue = '15px 15px 15px 0';
    }

    
    const openProject = (url) => {
        window.open(url, '__blank')
    }

    return (
        <HStack
            flex={1}
            p={{ base: 3, sm: 6 }}
            bg={useColorModeValue('gray.300', 'gray.800')}
            spacing={5}
            rounded="lg"
            alignItems="center"
            pos="relative"
            _before={{
                content: `""`,
                w: '0',
                h: '0',
                borderColor: `transparent ${useColorModeValue('#edf2f6', '#1a202c')} transparent`,
                borderStyle: 'solid',
                borderWidth: borderWidthValue,
                position: 'absolute',
                left: leftValue,
                right: rightValue,
                display: 'block'
            }}
        >
            <Box>
                <Image
                    src={img}
                    alt={title}
                    borderRadius='lg'
                />
                <Text fontSize="md" color={isEvenId ? 'primary.800' : 'blue.700'}>
                    {date}
                </Text>
                <VStack spacing={2} mb={3} textAlign="left">
                    <chakra.h1 fontSize="xl" lineHeight={1.2} fontWeight="bold" w="100%">
                        {title}
                    </chakra.h1>
                    <Text fontSize="sm">{description}</Text>
                </VStack>
                <HStack>
                {
                    id === 1 && (
                        <>
                            <Tag variant={'subtle'}>Vue 3</Tag>
                            <Tag variant={'subtle'}>Nest js</Tag>
                        </>
                    ) || id === 2 && (
                        <>
                         <Tag variant={'subtle'}>Vue 2</Tag>
                            <Tag variant={'subtle'}>Laravel</Tag>
                        </>
                    )
                }
                </HStack>
                <IconButton
                    onClick={ () => openProject(link) }
                    variant={'outline'}
                    className={'btn-open-project'}
                    size={'sm'}
                    colorScheme='primary'
                    _hover={{ color: 'primary.700', bgColor: 'primary.50', border: '1px solid', borderColor: 'primary.700' }}>
                        <FaLink size={24} />
                    </IconButton>
            </Box>
        </HStack>
    );
};

const LineWithDot = () => {
    return (
        <Flex
            pos="relative"
            alignItems="center"
            mr={{ base: '40px', md: '40px' }}
            ml={{ base: '0', md: '40px' }}
        >
            <chakra.span
                position="absolute"
                left="50%"
                height="calc(100% + 10px)"
                border="1px solid"
                borderColor={useColorModeValue('gray.400', 'gray.700')}
                top="0px"
            ></chakra.span>
            <Box pos="relative" p="10px">
                <Box
                    pos="absolute"
                    top="0"
                    left="0"
                    bottom="0"
                    right="0"
                    width="100%"
                    height="100%"
                    backgroundSize="cover"
                    backgroundRepeat="no-repeat"
                    backgroundPosition="center center"
                    bg={useColorModeValue('primary.800', 'gray.200')}
                    borderRadius="100px"
                    backgroundImage="none"
                    opacity={1}
                ></Box>
            </Box>
        </Flex>
    );
};

const EmptyCard = () => {
    return <Box flex={{ base: 0, md: 1 }} p={{ base: 0, md: 6 }} bg="transparent"></Box>;
};

export default Projects;