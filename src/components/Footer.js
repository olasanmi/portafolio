import { ButtonGroup, Container, IconButton, Stack, Text } from '@chakra-ui/react'
import * as React from 'react'
import { FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa'

export const Footer = () => {
  const currentYear = new Date().getFullYear();

  return (
    <Container maxW={'100%'} as="footer" role="contentinfo" py={{ base: '8', md: '8' }} bg='white' color={'primary.800'}>
      <Stack justify="center" spacing={{ base: '4', md: '5' }}>
        <Text textAlign={'center'} fontSize='20px' fontWeight={'bold'} color='gray.600'>{`Marcos Hernández © ${currentYear}`}</Text>
      </Stack>
    </Container>
  );
};
