import { Box, ButtonGroup, Container, Flex, Heading, HStack, IconButton, Image, Tag, Text, VStack } from '@chakra-ui/react'
import React from 'react'
import { FaEnvelope, FaFacebook, FaGitlab, FaInstagram, FaLinkedin, FaPhone } from 'react-icons/fa'
import Education from './Education'
import marcos from '../images/marcos.png'
 

export default function Banner() {
    const openRrSsFacebook = (url) => {
        window.open('https://web.facebook.com/Maalhequ95/', '__blank')
    }

    const openRrSsInstagram = () => {
        window.open('https://www.instagram.com/maalhequi95/', '__blank')
    }

    return (
        <Container maxW={'7xl'} alignItems='center' my={5}>
            <Flex width={'100%'} maxW={['full', 'full', '7xl']} justifyContent='center' alignItems={'center'} flexDir={['column', 'column', 'row', 'row']}>
                <Flex width={['md', 'md', '2xl']} flexDir={'column'} justifyContent='space-between' bgColor={'white'} color='black' maxHeight='2xl' minHeight={'4xl'} borderRadius='3xl' p='10' mb={10}>
                    <Box>
                        <Flex alignItems={'center'} justifyContent='center'>
                            <Image borderColor={'gray.100'} src={marcos} width='32' />
                        </Flex>
                        <Heading textAlign={'center'}>Marcos Hernandez</Heading>
                        <Text fontSize={'sm'} textAlign='center'>
                        Computer Engineer with more than 6 years of experience, specialized in software development and technological project management. My focus is on creating innovative and efficient solutions that drive project success.
                        </Text>
                        <VStack my={4}>
                            <Tag px={5} variant={'subtle'} colorScheme='primary'>Full Stack Web Developer</Tag>
                            <Tag px={5} variant={'subtle'} colorScheme='primary'>App Developer</Tag>
                        </VStack>
                        <Box orientation='horizontal' bgColor='gray.300' my={4} height={0.2} />
                        {/* Icons  */}
                        <VStack spacing={6} alignItems='start'>
                            <HStack>
                                <Box me={2}>
                                    <IconButton shadow={'md'} variant={'ghost'} border='1px solid' borderColor={'gray.300'}><FaEnvelope /></IconButton>
                                </Box>
                                <VStack alignItems={'start'} spacing={0.1}>
                                    <Text fontSize={'sm'}>Email</Text>
                                    <Text fontWeight={'600'}>maalhequi95@gmail.com</Text>
                                </VStack>
                            </HStack>
                            <HStack>
                                <Box me={2} >
                                    <IconButton shadow={'md'} variant={'ghost'} border='1px solid' borderColor={'gray.300'}><FaPhone /></IconButton>
                                </Box>
                                <VStack alignItems={'start'} spacing={0.1}>
                                    <Text fontSize={'sm'}>Phone</Text>
                                    <Text fontWeight={'600'}>+57 3244937535</Text>
                                </VStack>
                            </HStack>
                            <HStack>
                                <Box me={2}>
                                    <IconButton shadow={'md'} variant={'ghost'} border='1px solid' borderColor={'gray.300'}><FaGitlab /></IconButton>
                                </Box>
                                <VStack alignItems={'start'} spacing={0.1}>
                                    <Text fontSize={'sm'}>Gitlab</Text>
                                    <Text fontWeight={'600'}>@olasanmi</Text>
                                </VStack>
                            </HStack>
                            <HStack>
                                <Box me={2}>
                                    <IconButton shadow={'md'} variant={'ghost'} border='1px solid' borderColor={'gray.300'}><FaLinkedin /></IconButton>
                                </Box>
                                <VStack alignItems={'start'} spacing={0.1}>
                                    <Text fontSize={'sm'}>Linkedin</Text>
                                    <Text fontWeight={'600'}>marcos-hernandez-482291193</Text>
                                </VStack>
                            </HStack>
                        </VStack>
                    </Box>

                    <ButtonGroup justifyContent={'center'} alignItems={'center'} variant='ghost' spacing={1.9}>
                        <IconButton onClick={ openRrSsFacebook } _hover={{ color: 'primary.700', bgColor: 'primary.50', border: '1px solid', borderColor: 'primary.700' }}><FaFacebook size={24} /></IconButton>
                        <IconButton onClick={ openRrSsInstagram } _hover={{ color: 'primary.700', bgColor: 'primary.50', border: '1px solid', borderColor: 'primary.700' }}><FaInstagram size={24} /></IconButton>
                    </ButtonGroup>
                </Flex>
                <Box ms={['0', '0', '8']} flexDir={'column'} justifyContent='space-between' bgColor={'white'} color='black' maxHeight='4xl' scrollBehavior={'smooth'} minHeight={'4xl'} borderRadius='3xl' p='10' mb={10}>
                    <Heading>
                        Education
                    </Heading>
                    <Education />
                </Box>
            </Flex>
        </Container>
    )
}
